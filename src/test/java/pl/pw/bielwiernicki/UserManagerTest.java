package pl.pw.bielwiernicki;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import pl.pw.bielwiernicki.dao.BookDao;
import pl.pw.bielwiernicki.dao.CopyOfBookDao;
import pl.pw.bielwiernicki.manager.UserManager;
import pl.pw.bielwiernicki.model.Book;
import pl.pw.bielwiernicki.model.CopyOfBook;
import pl.pw.bielwiernicki.model.User;
import pl.pw.bielwiernicki.model.Visibility;

import java.util.Collection;

import static org.junit.Assert.assertEquals;

@ContextConfiguration(locations = {"classpath:testApplicationContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class UserManagerTest {

    @Autowired
    private UserManager userManager;
    @Autowired
    private BookFactory bookFactory;
    @Autowired
    private CopyOfBookDao copyOfBookDao;
    @Autowired
    private BookDao bookDao;

    @Test
         @Transactional
         public void sholdSaveBookWithAuthor() {
        User user = new User();
        user.setUsername("userA");
        Book book = bookFactory.buildSavedBook();

        //when

        user.getBooks().add(book);
        userManager.save(user);

        //then
        assertEquals(1, user.getBooks().size());

    }
    @Test
    @Transactional
    public void shouldSaveBooks() {
        User user = new User();
        user.setUsername("userB");
        Book book = bookFactory.buildSavedBook();

        //when

        user.getBooks().add(book);
        user.getBooks().add(book); //again
        userManager.save(user);

        //then
        assertEquals(1, user.getBooks().size());
        assertEquals(new Integer(2), user.getBooks().get(book).getNumberOfCopies());

    }

    @Test
    @Transactional
    public void shouldDeleteBookIfThereIsNoCopiesOfIt(){
        User user = new User();
        user.setUsername("userB");
        Book book = bookFactory.buildSavedBook();
        user.getBooks().add(book);
        user.getBooks().add(book); //again
        userManager.save(user);

        //when
        user.getBooks().delete(book);
        user.getBooks().delete(book);
        userManager.save(user);


        //then
        assertEquals(0, user.getBooks().size());
        Collection<CopyOfBook> allCopies = copyOfBookDao.getAll();
        assertEquals(0, allCopies.size());
        Collection<Book> all = bookDao.getAll();
        assertEquals(1, all.size());


    }

    @Test
    @Transactional
    public void DOSOMETHNG(){
        User user = new User();
        user.setUsername("userB");
        Book book = bookFactory.buildSavedBook();

    }


}
