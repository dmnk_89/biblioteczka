package pl.pw.bielwiernicki;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.pw.bielwiernicki.manager.BookManager;
import pl.pw.bielwiernicki.model.Book;


@Component
public class BookFactory {

    @Autowired
    private BookManager bookManager;

    public Book buildSavedBook() {
        Book book = new Book();
        book.setTitle("Title");

        bookManager.save(book);
        book = bookManager.get(book.getId());

        return book;
    }

}
