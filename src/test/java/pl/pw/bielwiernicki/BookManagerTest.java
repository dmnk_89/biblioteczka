package pl.pw.bielwiernicki;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import pl.pw.bielwiernicki.dao.CopyOfBookDao;
import pl.pw.bielwiernicki.manager.BookManager;
import pl.pw.bielwiernicki.manager.UserManager;
import pl.pw.bielwiernicki.model.*;

import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;

@ContextConfiguration(locations = {"classpath:testApplicationContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class BookManagerTest {

    @Autowired
    private BookManager manager;
    @Autowired
    private UserManager userManager;

    @Autowired
    private BookFactory bookFactory;

    @Autowired
    private CopyOfBookDao copyOfBookDao;
    @Test
    @Transactional
    public void sholdSaveBookWithAuthor() {
        //given
        Author author = new Author();
        author.setFirstName("A");
        author.setLastName("B");

        Book book = new Book();
        book.setTitle("Title");
        book.addAuthor(author);

        //when
        manager.save(book);
        Collection<Book> all = manager.getAll();

        //than
        assertEquals(1, all.size());

        Book founded = all.iterator().next();
        assertEquals(book.getTitle(), founded.getTitle());

        assertEquals(1, founded.getAuthors().size());

        Author a = founded.getAuthors().iterator().next();
        assertEquals(author.getFirstName(), a.getFirstName());
        assertEquals(author.getLastName(), a.getLastName());


    }


    @Test
    @Transactional
    public void shouldDeleteBook() {
        //given
        Author author = new Author();
        author.setFirstName("A");
        author.setLastName("B");

        Book book = new Book();
        book.setTitle("Title");
        book.addAuthor(author);

        //when
        manager.save(book);
        manager.delete(book);
        Collection<Book> all = manager.getAll();

        //than
        assertEquals(0, all.size());
    }

    @Test
    @Transactional
    public void testSomething(){
        User u1 = new User();
        User u2 = new User();

        Book b = bookFactory.buildSavedBook();

        u1.getBooks().add(b);
        u2.getBooks().add(b);

        userManager.save(u1);
        userManager.save(u2);

        List<CopyOfBook> all = manager.getAllCopies();
        assertEquals(1,all.size());


        CopyOfBook shouldHaveTwo = all.get(0);
        assertEquals(new Integer(2), shouldHaveTwo.getNumberOfCopies());

        Collection<CopyOfBook> allCopies = copyOfBookDao.getAll();
        assertEquals(2,allCopies.size());
    }
    @Test
    @Transactional
    public void testVisibility(){
        User u1 = new User();
        User u2 = new User();

        Book b = bookFactory.buildSavedBook();

        u1.getBooks().add(b);
        u2.getBooks().add(b);

        u2.getBooks().get(b).setVisibility(Visibility.NOBODY);

        userManager.save(u1);
        userManager.save(u2);

        List<CopyOfBook> all = manager.getAllCopies();
        assertEquals(1,all.size());

        CopyOfBook shouldHaveOne = all.get(0);
        assertEquals(new Integer(1),shouldHaveOne.getNumberOfCopies());

        Collection<CopyOfBook> allCopies = copyOfBookDao.getAll();
        assertEquals(2,allCopies.size());
    }

}
