package pl.pw.bielwiernicki;

import org.apache.wicket.util.tester.WicketTester;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.pw.bielwiernicki.model.User;
import pl.pw.bielwiernicki.web.Biblioteczka;
import pl.pw.bielwiernicki.web.BiblioteczkaSession;
import pl.pw.bielwiernicki.web.pages.HomePage;

import javax.persistence.OneToMany;

/**
 * Simple pages using the WicketTester
 *
 * @author rjansen
 */
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class TestHomePage {
    private WicketTester tester;

    @Autowired
    Biblioteczka app;

    @Before
    public void setUp() {
        OneToMany.class.getProtectionDomain().getCodeSource().getLocation();
        tester = new WicketTester(app);
    }

    @Test
    public void testRenderBasePage() {
        //given
        BiblioteczkaSession.get().setUser(new User());

        //when
        tester.startPage(HomePage.class);

        //then
        tester.assertRenderedPage(HomePage.class);
//        tester.assertComponent("logout", Link.class);
    }
}
