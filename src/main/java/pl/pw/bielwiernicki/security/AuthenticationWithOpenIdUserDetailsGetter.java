package pl.pw.bielwiernicki.security;


import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.openid.OpenIDAuthenticationToken;
import pl.pw.bielwiernicki.manager.BookManager;
import pl.pw.bielwiernicki.manager.UserManager;
import pl.pw.bielwiernicki.model.User;

public class AuthenticationWithOpenIdUserDetailsGetter implements UserDetailsService {
    private UserManager userManager;
    private BookManager bookManager;

    public AuthenticationWithOpenIdUserDetailsGetter(UserManager userManager, BookManager bookManager) {
        this.userManager = userManager;
        this.bookManager = bookManager;
    }

    /**
     * Probuje znalezc w bazie speakera na podstawie open id, jesli znajdzie
     * umieszcza go w SecurityContext. W przeciwnym wypadku rzuca wyjatek ktory
     * (patrz security.xml) powoduje wywolanie klasy obslugujacej nieudane
     * logowanie.
     */
    // @Override
    public UserDetails loadUserByUsername(String openIdLogin) throws UsernameNotFoundException {

        User user = userManager.findByOpenIdLogin(openIdLogin);

        throwExceptionIfNotFound(user, openIdLogin);

        user.setBooks(bookManager.getBookSet(user.getBooks()));

        AuthenticationUserDetails userDetails = new AuthenticationUserDetails(user);
        OpenIDAuthenticationToken openIdAuthenticationToken = new OpenIDAuthenticationToken(userDetails,
                userDetails.getAuthorities(), null, null);
        SecurityContextHolder.getContext().setAuthentication(openIdAuthenticationToken);

        return new AuthenticationUserDetails(user);
    }

    private void throwExceptionIfNotFound(Object object, String openIdLogin) {
        if (object == null) {
            throw new UsernameNotFoundException("User with open id login " + openIdLogin + " has not been found.");
        }
    }
}