package pl.pw.bielwiernicki.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import pl.pw.bielwiernicki.model.User;

import java.util.Collection;
import java.util.HashSet;

public class AuthenticationUserDetails implements UserDetails {
    private static final long serialVersionUID = -8297478803363997090L;

    private final String login;
    private final boolean enabled;
    private User user;
    private HashSet<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();

    public AuthenticationUserDetails(User user) {
        this.login = user.getMail();
        this.enabled = true;
        this.grantedAuthorities.addAll(new HashSet<GrantedAuthority>());
        this.user = user;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return grantedAuthorities;
    }

    public String getPassword() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public User getUser() {
        return user;
    }


    @Override
    public String toString() {
        return login;
    }
}
