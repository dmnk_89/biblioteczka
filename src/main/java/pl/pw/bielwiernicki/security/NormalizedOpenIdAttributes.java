package pl.pw.bielwiernicki.security;

import java.io.Serializable;

@SuppressWarnings("serial")
public class NormalizedOpenIdAttributes implements Serializable {

    private String userLocalIdentifier;
    private String emailAddress;
    private String firstName;
    private String lastName;
    private String fullName;
    private String loginReplacement;

    public NormalizedOpenIdAttributes(String userLocalIdentifier, String emailAddress, String firstName,
                                      String lastName, String fullName, String loginReplacement) {
        this.userLocalIdentifier = userLocalIdentifier;
        this.emailAddress = emailAddress;
        this.firstName = firstName;
        this.lastName = lastName;
        this.fullName = fullName;
        this.loginReplacement = loginReplacement;
    }

    public String getUserLocalIdentifier() {
        return userLocalIdentifier;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getFullName() {
        return fullName;
    }

    public String getLoginReplacement() {
        return loginReplacement;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}