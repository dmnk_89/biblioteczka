package pl.pw.bielwiernicki.dao;

import pl.pw.bielwiernicki.model.BookSet;

/**
 * Created with IntelliJ IDEA.
 * User: bield
 * Date: 13.12.12
 * Time: 20:23
 * To change this template use File | Settings | File Templates.
 */

public interface BookSetDao extends GenericEntityDao<BookSet>{
}
