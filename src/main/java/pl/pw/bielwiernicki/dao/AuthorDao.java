package pl.pw.bielwiernicki.dao;

import pl.pw.bielwiernicki.model.Author;

public interface AuthorDao extends GenericEntityDao<Author> {
}
