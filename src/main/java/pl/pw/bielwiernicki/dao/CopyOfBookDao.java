package pl.pw.bielwiernicki.dao;

import pl.pw.bielwiernicki.model.CopyOfBook;

import java.util.Collection;


public interface CopyOfBookDao extends GenericEntityDao<CopyOfBook>{

    public Collection<CopyOfBook> getAllPublic();
}