package pl.pw.bielwiernicki.dao;

import pl.pw.bielwiernicki.model.UserRole;

/**
 * Interface for retrieving the possible roles for usersmanagement
 *
 * @author rjansen
 */
public interface UserRoleDao extends GenericEntityDao<UserRole> {

}
