package pl.pw.bielwiernicki.dao.impl;


import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.springframework.stereotype.Repository;
import pl.pw.bielwiernicki.dao.BookDao;
import pl.pw.bielwiernicki.model.Book;

import java.util.List;

@Repository("bookDao")
public class BookDaoImpl extends GenericEntityDaoImpl<Book> implements BookDao {

    protected BookDaoImpl() {
        super(Book.class);
    }

    public List find(String input) {
        DetachedCriteria criteria = DetachedCriteria.forClass(Book.class);
        criteria.add(Property.forName("title").like("%"+input+"%"));
        return getHibernateTemplate().findByCriteria(criteria);
    }
}
