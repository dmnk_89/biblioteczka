package pl.pw.bielwiernicki.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.pw.bielwiernicki.dao.BookSetDao;
import pl.pw.bielwiernicki.dao.CopyOfBookDao;
import pl.pw.bielwiernicki.model.BookSet;
import pl.pw.bielwiernicki.model.CopyOfBook;

import java.util.Collection;

@Repository
public class BookSetDaoImpl extends GenericEntityDaoImpl<BookSet> implements BookSetDao {

    @Autowired
    CopyOfBookDao copyOfBookDao;

    protected BookSetDaoImpl() {
        super(BookSet.class);
    }

    @Override
    public void save(BookSet item) {
        for (CopyOfBook cp : item) {
            copyOfBookDao.save(cp);
        }
        super.save(item);
    }

    @Override
    public void update(BookSet item) {
        for (CopyOfBook cp : item) {
            copyOfBookDao.update(cp);
        }
        super.update(item);
    }
}
