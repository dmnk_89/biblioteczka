package pl.pw.bielwiernicki.dao.impl;

import org.springframework.stereotype.Repository;
import pl.pw.bielwiernicki.dao.BookDao;
import pl.pw.bielwiernicki.dao.OrganisationDao;
import pl.pw.bielwiernicki.model.Book;
import pl.pw.bielwiernicki.model.Organisation;

/**
 * Created with IntelliJ IDEA.
 * User: wiernicd
 * Date: 04.12.12
 * Time: 17:44
 * To change this template use File | Settings | File Templates.
 */
@Repository ("organisationDao")
public class OrganisationDAOImpl extends GenericEntityDaoImpl<Organisation> implements OrganisationDao {


    protected OrganisationDAOImpl() {
        super(Organisation.class);
    }
}
