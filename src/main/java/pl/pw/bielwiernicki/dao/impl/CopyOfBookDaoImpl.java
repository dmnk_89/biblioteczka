package pl.pw.bielwiernicki.dao.impl;


import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import pl.pw.bielwiernicki.dao.CopyOfBookDao;
import pl.pw.bielwiernicki.model.CopyOfBook;
import pl.pw.bielwiernicki.model.Visibility;

import java.util.Collection;

@Repository
public class CopyOfBookDaoImpl extends GenericEntityDaoImpl<CopyOfBook> implements CopyOfBookDao{

    protected CopyOfBookDaoImpl() {
        super(CopyOfBook.class);
    }

    public Collection<CopyOfBook> getAllPublic() {
        return getSession().createCriteria(CopyOfBook.class).add(Restrictions.eq("visibility", Visibility.EVERYONE)).list();
    }
}
