package pl.pw.bielwiernicki.dao.impl;


import org.springframework.stereotype.Repository;
import pl.pw.bielwiernicki.dao.AuthorDao;
import pl.pw.bielwiernicki.model.Author;

@Repository("authorDao")
public class AuthorDaoImpl extends GenericEntityDaoImpl<Author> implements AuthorDao {

    protected AuthorDaoImpl() {
        super(Author.class);
    }
}
