package pl.pw.bielwiernicki.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import pl.pw.bielwiernicki.dao.UserDao;
import pl.pw.bielwiernicki.model.User;

import java.util.List;

/**
 * @author rjansen
 */
@Repository("userDao")
public class UserDaoImpl extends GenericEntityDaoImpl<User> implements UserDao {

    /**
     * The logging class.
     */
    private static final Log LOG = LogFactory.getLog(UserDaoImpl.class);

    /**
     * Constructor for UserDaoImpl.
     */
    public UserDaoImpl() {
        super(User.class);
    }

    /**
     * Will return the user based on the username
     *
     * @param username the username
     * @return the user if found, else null
     */
    @SuppressWarnings("unchecked")
    public User get(String username) {
        LOG.debug("get user for username " + username);
        User foundUser = null;
        DetachedCriteria searchCriteria = DetachedCriteria.forClass(User.class);
        searchCriteria.add(Restrictions.eq("username", username));
        List<User> values = (List<User>) getHibernateTemplate().findByCriteria(
                searchCriteria);
        if (values != null && values.size() >= 1) {
            foundUser = values.get(0);
        }

        return foundUser;
    }


}
