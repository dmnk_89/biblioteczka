package pl.pw.bielwiernicki.dao.impl;

import org.springframework.stereotype.Repository;
import pl.pw.bielwiernicki.dao.UserRoleDao;
import pl.pw.bielwiernicki.model.UserRole;

@Repository("userRoleDao")
public class UserRoleDaoImpl extends GenericEntityDaoImpl<UserRole> implements
        UserRoleDao {

    protected UserRoleDaoImpl() {
        super(UserRole.class);
    }

}
