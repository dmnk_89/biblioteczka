package pl.pw.bielwiernicki.dao;

import pl.pw.bielwiernicki.model.Book;

import java.util.ArrayList;
import java.util.List;


public interface BookDao extends GenericEntityDao<Book> {
    List find(String input);
}
