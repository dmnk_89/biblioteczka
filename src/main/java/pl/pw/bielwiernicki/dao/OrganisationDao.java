package pl.pw.bielwiernicki.dao;

import pl.pw.bielwiernicki.model.Organisation;

/**
 * Created with IntelliJ IDEA.
 * User: wiernicd
 * Date: 04.12.12
 * Time: 17:45
 * To change this template use File | Settings | File Templates.
 */
public interface OrganisationDao extends GenericEntityDao<Organisation>{
}
