package pl.pw.bielwiernicki.web;

import org.apache.wicket.Session;
import org.apache.wicket.protocol.http.WebSession;
import org.apache.wicket.request.Request;
import pl.pw.bielwiernicki.model.User;

public class BiblioteczkaSession extends WebSession {
    private User user;

    public BiblioteczkaSession(Request request) {
        super(request);

    }

    public static BiblioteczkaSession get() {
        return (BiblioteczkaSession) Session.get();
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public boolean hasUser() {
        return user != null;
    }
}
