package pl.pw.bielwiernicki.web;

import org.apache.wicket.Page;
import org.apache.wicket.Session;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.Response;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import pl.pw.bielwiernicki.web.pages.HomePage;
import pl.pw.bielwiernicki.web.pages.UserHomePage;
import pl.pw.bielwiernicki.web.pages.books.AddBookPage;
import pl.pw.bielwiernicki.web.pages.books.AddCopyOfBookPage;
import pl.pw.bielwiernicki.web.pages.books.BookListPage;
import pl.pw.bielwiernicki.web.pages.books.UserBooksListPage;
import pl.pw.bielwiernicki.web.pages.organisations.CreateOrganisationPage;
import pl.pw.bielwiernicki.web.pages.usersmanagement.DeleteUserPage;
import pl.pw.bielwiernicki.web.pages.usersmanagement.RegisterUserPage;


@Component
public class Biblioteczka extends WebApplication implements ApplicationContextAware {


    @SpringBean
    private ApplicationContext ctx;

    @Override
    protected void init() {
        super.init();
        getComponentInstantiationListeners().add(new SpringComponentInjector(this, ctx));
        mountPage("/register", RegisterUserPage.class);
        mountPage("/home", UserHomePage.class);
        mountPage("/user/delete", DeleteUserPage.class);
        mountPage("/user/books", UserBooksListPage.class);
        mountPage("/user/books/add", AddCopyOfBookPage.class);
        mountPage("/books", BookListPage.class);
        mountPage("/books/add", AddBookPage.class);
        mountPage("/orgs/create", CreateOrganisationPage.class);
//        mountPage("/authors/add", AddAuthorForm.class);
    }

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.ctx = applicationContext;
    }

    @Override
    public Class<? extends Page> getHomePage() {
        return HomePage.class;
    }

    @Override
    public Session newSession(Request request, Response response) {
        return new BiblioteczkaSession(request);
    }
}

