package pl.pw.bielwiernicki.web.pages.usersmanagement;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.spring.injection.annot.SpringBean;
import pl.pw.bielwiernicki.manager.UserManager;
import pl.pw.bielwiernicki.model.User;
import pl.pw.bielwiernicki.security.NormalizedOpenIdAttributes;
import pl.pw.bielwiernicki.web.BiblioteczkaSession;
import pl.pw.bielwiernicki.web.pages.UserHomePage;

import javax.servlet.http.HttpServletRequest;


public class RegisterUserPage extends WebPage {

    @SpringBean
    UserManager manager;

    public RegisterUserPage() {
        NormalizedOpenIdAttributes attributes = getOpenIdAttributes();
        User user = createUser(attributes);
        manager.save(user);

        getSession().info("Konto utworzone");
        BiblioteczkaSession.get().setUser(user);
        setResponsePage(UserHomePage.class);
    }

    private User createUser(NormalizedOpenIdAttributes attributes) {
        User user = new User();
        user.setMail(attributes.getEmailAddress());
        user.setUsername(attributes.getUserLocalIdentifier());
        user.setName(attributes.getFullName());
        return user;
    }

    private NormalizedOpenIdAttributes getOpenIdAttributes() {
        return (NormalizedOpenIdAttributes) ((HttpServletRequest) getRequest()
                .getContainerRequest())
                .getSession().getAttribute("USER_OPENID_CREDENTIAL");
    }
}
