package pl.pw.bielwiernicki.web.pages.books;


import org.apache.wicket.extensions.ajax.markup.html.autocomplete.DefaultCssAutoCompleteTextField;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.convert.IConverter;
import pl.pw.bielwiernicki.manager.BookManager;
import pl.pw.bielwiernicki.manager.UserManager;
import pl.pw.bielwiernicki.model.Book;
import pl.pw.bielwiernicki.model.User;
import pl.pw.bielwiernicki.web.BiblioteczkaSession;
import pl.pw.bielwiernicki.web.pages.BasePage;
import pl.pw.bielwiernicki.web.pages.books.lists.BookListView;

import java.util.*;

public class AddCopyOfBookPage extends BasePage {

    @SpringBean
    BookManager bookManager;
    @SpringBean
    UserManager userManager;
    private TextField input;
    private TextField quantity;
    private Collection<Book> books = Collections.EMPTY_LIST;
    private BookListView booksList;


    public AddCopyOfBookPage() {


        books = bookManager.getAll();

        input = new DefaultCssAutoCompleteTextField<Book>("input", new Model<Book>()) {
            @Override
            protected Iterator<Book> getChoices(String input) {
                return books.iterator();
            }

            @Override
            public <C> IConverter<C> getConverter(Class<C> type) {
                if (Book.class.isAssignableFrom(type)) {
                    return (IConverter<C>) new IConverter<Book>() {
                        public Book convertToObject(String s, Locale locale) {
                            List<Book> Cbooks = bookManager.find(s);
                            if (!Cbooks.isEmpty())
                                return bookManager.find(s).get(0);
                            return new Book("");
                        }

                        public String convertToString(Book c, Locale locale) {
                            if (c != null && c.getTitle()!=null)
                                return c.getTitle();
                            return "";
                        }
                    };
                }
                return super.getConverter(type);    //To change body of overridden methods use File | Settings | File Templates.
            }
        };
        input.setType(Book.class);

        quantity = new TextField<Integer>("quantity", new Model<Integer>(1));
        quantity.setType(Integer.class);

        Form form = new Form<Void>("form") {
            @Override
            protected void onSubmit() {
                if (input.getModelObject() != null) {
                    User user = BiblioteczkaSession.get().getUser();
                    user.getBooks().add((Book) input.getModelObject(), (Integer) quantity.getModelObject());
                    userManager.save(user);
                }
                super.onSubmit();
            }
        };
        form.add(quantity);
        form.add(input);

        add(form);


    }


}
