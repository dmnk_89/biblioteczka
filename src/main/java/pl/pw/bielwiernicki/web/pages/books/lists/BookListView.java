package pl.pw.bielwiernicki.web.pages.books.lists;

import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.CompoundPropertyModel;
import pl.pw.bielwiernicki.model.Book;
import pl.pw.bielwiernicki.model.CopyOfBook;
import pl.pw.bielwiernicki.web.pages.books.BookDetailsPage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class BookListView extends ListView<Book> {


    public BookListView(String id, Collection<Book> books) {
        super(id,  new ArrayList<Book>(books));
    }

    @Override
    protected void populateItem(final ListItem<Book> item) {
        item.setModel(new CompoundPropertyModel<Book>(item.getModel()));
        item.add(new Label("title").add(new AjaxEventBehavior("onclick") {
            @Override
            protected void onEvent(AjaxRequestTarget target) {
                setResponsePage(new BookDetailsPage(item.getModelObject()));
            }
        }) );
        item.add(new AuthorsListView("authors"));
    }

    public void setBooks(BookListView booksList) {
        //To change body of created methods use File | Settings | File Templates.
    }
}
