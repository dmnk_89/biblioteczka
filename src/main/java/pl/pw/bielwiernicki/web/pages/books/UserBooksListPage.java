package pl.pw.bielwiernicki.web.pages.books;

import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.spring.injection.annot.SpringBean;
import pl.pw.bielwiernicki.manager.UserManager;
import pl.pw.bielwiernicki.model.Book;
import pl.pw.bielwiernicki.model.BookSet;
import pl.pw.bielwiernicki.model.CopyOfBook;
import pl.pw.bielwiernicki.model.User;
import pl.pw.bielwiernicki.web.Biblioteczka;
import pl.pw.bielwiernicki.web.BiblioteczkaSession;
import pl.pw.bielwiernicki.web.pages.BasePage;
import pl.pw.bielwiernicki.web.pages.books.lists.BookCopyListView;
import pl.pw.bielwiernicki.web.pages.books.lists.BookListView;

import java.util.Collections;
import java.util.Map;
import java.util.Set;


public class UserBooksListPage extends BasePage{

    @SpringBean
    UserManager manager;

    public UserBooksListPage() {
        User user = BiblioteczkaSession.get().getUser();
        BookSet books = user.getBooks();


        final WebMarkupContainer component = new WebMarkupContainer("container");
        add(component);
        component.setOutputMarkupId(true);
        component.add(new BookCopyListView("books", books.getColecton()));
        add(new BookmarkablePageLink("button", AddCopyOfBookPage.class) );
    }
}
