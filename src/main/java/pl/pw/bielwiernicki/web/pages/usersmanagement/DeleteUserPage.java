package pl.pw.bielwiernicki.web.pages.usersmanagement;

import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import pl.pw.bielwiernicki.manager.UserManager;
import pl.pw.bielwiernicki.security.AuthenticationUserDetails;
import pl.pw.bielwiernicki.web.BiblioteczkaSession;
import pl.pw.bielwiernicki.web.pages.HomePage;


public class DeleteUserPage extends WebPage {

    @SpringBean
    UserManager manager;

    public DeleteUserPage() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            if (authentication.getPrincipal() instanceof AuthenticationUserDetails) {
                AuthenticationUserDetails userDetails = (AuthenticationUserDetails) authentication.getPrincipal();
                BiblioteczkaSession.get().setUser(userDetails.getUser());
            }
        }
        if (!BiblioteczkaSession.get().hasUser())
            throw new RestartResponseAtInterceptPageException(HomePage.class);


        manager.delete(BiblioteczkaSession.get().getUser());

        setResponsePage(HomePage.class);
        getSession().info("Konto usunięte");


    }
}
