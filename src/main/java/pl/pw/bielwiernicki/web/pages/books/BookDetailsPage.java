package pl.pw.bielwiernicki.web.pages.books;


import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import pl.pw.bielwiernicki.model.Book;
import pl.pw.bielwiernicki.web.pages.BasePage;
import pl.pw.bielwiernicki.web.pages.books.lists.AuthorsListView;

public class BookDetailsPage extends BasePage{

    public BookDetailsPage(final Book book) {
        setDefaultModel(new CompoundPropertyModel<Book>(book));
        add(new Label("title"));
        add(new Link("edit") {
            @Override
            public void onClick() {
                setResponsePage(new AddBookPage(book));
            }
        });
        add(new AuthorsListView("authors"));
    }
}
