package pl.pw.bielwiernicki.web.pages.books;

import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.spring.injection.annot.SpringBean;
import pl.pw.bielwiernicki.manager.BookManager;
import pl.pw.bielwiernicki.model.Book;
import pl.pw.bielwiernicki.web.pages.BasePage;
import pl.pw.bielwiernicki.web.pages.books.lists.BookListView;

import java.util.ArrayList;
import java.util.List;


public class BookListPage extends BasePage {

    @SpringBean
    BookManager manager;
    private List<Book> books;

    public BookListPage() {

        books = new ArrayList<Book>(manager.getAll());
        add(new BookListView("books", books));
        add(new BookmarkablePageLink("add book", AddBookPage.class));
    }


}
