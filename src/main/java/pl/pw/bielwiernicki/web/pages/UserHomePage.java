package pl.pw.bielwiernicki.web.pages;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.spring.injection.annot.SpringBean;
import pl.pw.bielwiernicki.manager.BookManager;
import pl.pw.bielwiernicki.model.CopyOfBook;

import java.util.List;

public class UserHomePage extends BasePage {

    @SpringBean
    private BookManager bookManager;

    public UserHomePage() {
        List<CopyOfBook> copies = bookManager.getAllCopies();
        add(new ListView<CopyOfBook>("books",copies) {
            @Override
            protected void populateItem(ListItem<CopyOfBook> item) {
                item.add(new Label("title",item.getModelObject().getBook().getTitle()));
                item.add(new Label("quantity",item.getModelObject().getNumberOfCopies().toString()));
            }
        });
    }
}
