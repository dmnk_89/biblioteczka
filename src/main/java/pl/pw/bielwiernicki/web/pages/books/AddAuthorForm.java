package pl.pw.bielwiernicki.web.pages.books;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import pl.pw.bielwiernicki.model.Author;

public abstract class AddAuthorForm extends Panel {

    public AddAuthorForm(String id) {
        super(id);
        Form<Author> form = new AuthorForm("form", new Author());
        form.add(new TextField<String>("firstName"));
        form.add(new TextField<String>("lastName"));
        form.add(new AjaxSubmitLink("submit") {
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form form) {
                Author o = (Author) form.getModelObject();
                if (o != null){
                    AddAuthorForm.this.onSubmit(target, o);
                }
            }
        });
        add(form);
    }

    public abstract void onSubmit(AjaxRequestTarget target, Author author);

    private class AuthorForm extends Form<Author> {

        public AuthorForm(String id, Author author) {
            super(id, new CompoundPropertyModel<Author>(author));
            setModelObject(author);
        }

//        @Override
//        protected void onSubmit() {
//            Author o = getModelObject();
//            AddAuthorForm.this.onSubmit(o);
//        }
    }
}
