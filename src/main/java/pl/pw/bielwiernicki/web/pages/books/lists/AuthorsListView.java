package pl.pw.bielwiernicki.web.pages.books.lists;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.Model;
import pl.pw.bielwiernicki.model.Author;

import java.util.List;


public class AuthorsListView extends ListView<Author> {

    public AuthorsListView(String id) {
        super(id);
    }

    public AuthorsListView(String id, List<Author> authors) {
        super(id, authors);
    }


    @Override
    protected void populateItem(ListItem<Author> item) {
        item.setModel(new CompoundPropertyModel<Author>(item.getModelObject()));
        item.add(new Label("firstName"));
        item.add(new Label("lastName"));
    }
}