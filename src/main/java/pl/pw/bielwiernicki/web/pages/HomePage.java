package pl.pw.bielwiernicki.web.pages;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.panel.FeedbackPanel;


public class HomePage extends WebPage {
    public HomePage() {
        add(new FeedbackPanel("feedback"));
    }
}
