package pl.pw.bielwiernicki.web.pages.books;


import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.SubmitLink;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import pl.pw.bielwiernicki.manager.BookManager;
import pl.pw.bielwiernicki.model.Author;
import pl.pw.bielwiernicki.model.Book;
import pl.pw.bielwiernicki.web.pages.BasePage;
import pl.pw.bielwiernicki.web.pages.books.lists.AuthorsListView;

public class AddBookPage extends BasePage {


    @SpringBean
    private BookManager bookManager;

//    private AuthorsView authors = new AuthorsView("authors");

    public AddBookPage() {
        this(new Book());

    }

    public AddBookPage(final Book book) {
        add(new BookForm("form", book));
        add(new AddAuthorForm("newAuthor") {
            @Override
            public void onSubmit(AjaxRequestTarget target, Author author) {
                book.addAuthor(new Author(author));
                author.setLastName("");
                author.setFirstName("");
                target.add(AddBookPage.this.get("form:authorsContainer"));
//                setResponsePage(new AddBookPage(book));
            }
        });
    }

    private class BookForm extends Form<Book> {

        public BookForm(String id, final Book book) {
            super(id, new CompoundPropertyModel<Book>(book));

            add(new TextField<String>("title"));
            add(new WebMarkupContainer("authorsContainer")
                    .add(new AuthorsListView("authors"))
                    .setOutputMarkupId(true)
            );
            add(new SubmitLink("submit"));
        }

        @Override
        protected void onSubmit() {
            Book book = getModelObject();
            bookManager.save(book);
            super.onSubmit();
            setResponsePage(new BookListPage());
        }
    }

}
