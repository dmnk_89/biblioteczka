package pl.pw.bielwiernicki.web.pages;

import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.Model;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import pl.pw.bielwiernicki.security.AuthenticationUserDetails;
import pl.pw.bielwiernicki.web.BiblioteczkaSession;
import pl.pw.bielwiernicki.web.pages.usersmanagement.DeleteUserPage;


public abstract class BasePage extends WebPage {

    protected FeedbackPanel feedback = new FeedbackPanel("feedback");

    public BasePage() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            if (authentication.getPrincipal() instanceof AuthenticationUserDetails) {
                AuthenticationUserDetails userDetails = (AuthenticationUserDetails) authentication.getPrincipal();
                BiblioteczkaSession.get().setUser(userDetails.getUser());
            }
        }
        if (!BiblioteczkaSession.get().hasUser())
            throw new RestartResponseAtInterceptPageException(HomePage.class);

        add(feedback);
        add(new Label("userWelcome", new Model<String>(BiblioteczkaSession.get().getUser().getFullName())));
        add(new LogoutLink("logout"));
    }


    private final class LogoutLink extends Link {

        private LogoutLink(String id) {
            super(id);
        }

        @Override
        public void onClick() {
            BiblioteczkaSession.get().invalidateNow();
            setResponsePage(HomePage.class);
        }
    }
}
