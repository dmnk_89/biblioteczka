package pl.pw.bielwiernicki.web.pages.books.lists;

import org.apache.wicket.ajax.AbstractDefaultAjaxBehavior;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.WebComponent;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;
import pl.pw.bielwiernicki.manager.BookManager;
import pl.pw.bielwiernicki.model.CopyOfBook;
import pl.pw.bielwiernicki.web.pages.books.BookDetailsPage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;


public class BookCopyListView extends ListView<CopyOfBook> {


    @SpringBean
    BookManager bookManager;

    public BookCopyListView(String id, Collection<CopyOfBook> books) {
        super(id,  new ArrayList<CopyOfBook>(books));
    }

    @Override
    protected void populateItem(final ListItem<CopyOfBook> item) {

        item.add(new Label("title",item.getModelObject().getBook().getTitle()).add(new AjaxEventBehavior("onclick") {
            @Override
            protected void onEvent(AjaxRequestTarget target) {
                setResponsePage(new BookDetailsPage(item.getModelObject().getBook()));
            }
        }) );


        item.add(new Label("quantity",new Model<Serializable>(item.getModelObject().getNumberOfCopies())));
        item.add(new Label("visibility",new Model<Serializable>(item.getModelObject().getVisibility())));
        item.add(new AjaxLink("change",new Model<Serializable>(item.getModelObject().getVisibility())) {
            @Override
            public void onClick(AjaxRequestTarget target) {
                target.add(BookCopyListView.this.getParent());
                item.getModelObject().swapVsibility();
                bookManager.save(item.getModelObject());
            }
        });
        item.add(new AuthorsListView("authors", item.getModelObject().getBook().getAuthors()));
    }


}
