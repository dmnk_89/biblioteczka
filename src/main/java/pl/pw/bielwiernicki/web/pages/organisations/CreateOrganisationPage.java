package pl.pw.bielwiernicki.web.pages.organisations;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.ComponentPropertyModel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.springframework.ui.Model;
import pl.pw.bielwiernicki.manager.OrganisationManager;
import pl.pw.bielwiernicki.model.Organisation;
import pl.pw.bielwiernicki.web.pages.BasePage;

/**
 * Created with IntelliJ IDEA.
 * User: wiernicd
 * Date: 04.12.12
 * Time: 17:18
 * To change this template use File | Settings | File Templates.
 */
public class CreateOrganisationPage extends BasePage{

    @SpringBean
    OrganisationManager organisationManager;

    public CreateOrganisationPage() {
        super();
        Organisation org = new Organisation();
        add(new CreateOrganizationForm("my_form", org));

    }

    private class CreateOrganizationForm extends Form<Organisation>{

        public CreateOrganizationForm(String id, Organisation org) {
            super(id);
            setModel(new CompoundPropertyModel<Organisation>(org));
            add(new TextField<String>("name"))    ;
            add(new TextField<String>("my_set_input", new org.apache.wicket.model.Model<String>("test")))    ;
        }

        @Override
        protected void onSubmit() {
            Organisation org = (Organisation) getDefaultModelObject();
            organisationManager.save(org);
            super.onSubmit();
        }
    }
}
