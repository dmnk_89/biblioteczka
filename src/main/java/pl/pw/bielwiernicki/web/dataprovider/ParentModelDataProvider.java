package pl.pw.bielwiernicki.web.dataprovider;

import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import pl.pw.bielwiernicki.manager.GenericEntityManager;
import pl.pw.bielwiernicki.model.ParentModel;

import java.util.Iterator;

@SuppressWarnings("serial")
public abstract class ParentModelDataProvider<T extends ParentModel> extends
        SortableDataProvider<T, String> {

    public Iterator<? extends T> iterator(long first, long count) {
        return getManager().getAll().iterator();
    }

    public long size() {
        return getManager().getAll().size();
    }

    public IModel<T> model(T object) {
        return new Model<T>(object);
    }

    /**
     * The manager for retrieving the entity values
     *
     * @return the manager
     */
    protected abstract GenericEntityManager<T> getManager();

}
