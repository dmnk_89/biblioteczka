package pl.pw.bielwiernicki.manager;

import pl.pw.bielwiernicki.model.User;
import pl.pw.bielwiernicki.model.UserRole;

import java.util.Collection;

/**
 * Interface of user manager.
 *
 * @author rjansen
 */

public interface UserManager extends GenericEntityManager<User> {

    /**
     * Return the entity with the defined id.
     *
     * @param username The username of the entity to fetch.
     * @return fetched entity with provided id.
     */
    User get(String username);

    /**
     * Get all user roles
     *
     * @return the user roles
     */
    public Collection<UserRole> getAllRoles();

    User findByOpenIdLogin(String openIdLogin);
}
