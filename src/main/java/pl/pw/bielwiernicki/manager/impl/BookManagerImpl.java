package pl.pw.bielwiernicki.manager.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.pw.bielwiernicki.dao.AuthorDao;
import pl.pw.bielwiernicki.dao.BookDao;
import pl.pw.bielwiernicki.dao.BookSetDao;
import pl.pw.bielwiernicki.dao.CopyOfBookDao;
import pl.pw.bielwiernicki.manager.BookManager;
import pl.pw.bielwiernicki.model.Author;
import pl.pw.bielwiernicki.model.Book;
import pl.pw.bielwiernicki.model.BookSet;
import pl.pw.bielwiernicki.model.CopyOfBook;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Repository("bookManager")
public class BookManagerImpl extends GenericEntityManagerImpl<Book> implements BookManager {

    private BookDao bookDao;

    private AuthorDao authorDao;

    private CopyOfBookDao copyOfBookDao;

    private BookSetDao bookSetDao;

    @Autowired
    public void setBookSetDao(BookSetDao bookSetDao) {
        this.bookSetDao = bookSetDao;
    }

    @Autowired
    public void setCopyOfBookDao(CopyOfBookDao copyOfBookDao) {
        this.copyOfBookDao = copyOfBookDao;
    }

    @Autowired
    public void setBookDao(BookDao bookDao) {
        setEntityDao(bookDao);
        this.bookDao = bookDao;
    }

    @Autowired
    public void setAuthorDao(AuthorDao authorDao) {
        this.authorDao = authorDao;
    }

    @Override
    public void save(Book book) {
        if (book == null) {
            return;
        }
        for (Author author : book.getAuthors()) {
            authorDao.save(author);
        }
        bookDao.save(book);

    }

    public List<Book> find(String input) {
        List<Book> books = bookDao.find(input);
        return books;
    }

    public List<CopyOfBook> getAllCopies() {
        Collection<CopyOfBook> allCopies = copyOfBookDao.getAllPublic();

        Multimap<Book, CopyOfBook> myMultimap = ArrayListMultimap.create();
        for (CopyOfBook copy : allCopies) {
            myMultimap.put(copy.getBook(), copy);
        }

        List<CopyOfBook> toReturn = new ArrayList<CopyOfBook>();
        for (Book b : myMultimap.keySet()) {
            CopyOfBook toAdd = new CopyOfBook(b, 0);
            for (CopyOfBook copy : myMultimap.get(b)) {
                toAdd.increment(copy.getNumberOfCopies());
            }
            toReturn.add(toAdd);
        }
        return toReturn;
    }

    public Collection<BookSet> getAllBookSets() {
        return bookSetDao.getAll();
    }

    public void save(CopyOfBook modelObject) {
        copyOfBookDao.save(modelObject);
    }

    public BookSet getBookSet(BookSet books) {
        for(BookSet set:getAllBookSets()){
            if(set.getId() .equals( books.getId()))   {
                return set;
            }
        }
        return books;
    }
}
