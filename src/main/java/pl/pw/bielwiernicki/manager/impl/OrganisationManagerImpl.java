package pl.pw.bielwiernicki.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pw.bielwiernicki.dao.OrganisationDao;
import pl.pw.bielwiernicki.manager.OrganisationManager;
import pl.pw.bielwiernicki.model.Organisation;

import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: wiernicd
 * Date: 04.12.12
 * Time: 17:48
 * To change this template use File | Settings | File Templates.
 */
@Service
public class OrganisationManagerImpl extends GenericEntityManagerImpl<Organisation> implements OrganisationManager {
    OrganisationDao organisationDao;

    @Autowired
    public void setOrganisationDao(OrganisationDao organisationDao) {
        super.setEntityDao(organisationDao);
        this.organisationDao = organisationDao;
    }
}
