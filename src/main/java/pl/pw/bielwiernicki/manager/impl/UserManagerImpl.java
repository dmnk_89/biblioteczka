package pl.pw.bielwiernicki.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pw.bielwiernicki.dao.BookSetDao;
import pl.pw.bielwiernicki.dao.CopyOfBookDao;
import pl.pw.bielwiernicki.dao.UserDao;
import pl.pw.bielwiernicki.dao.UserRoleDao;
import pl.pw.bielwiernicki.manager.UserManager;
import pl.pw.bielwiernicki.model.User;
import pl.pw.bielwiernicki.model.UserRole;

import java.util.Collection;

/**
 * Implementation of Manager to handle User data.
 *
 * @author rjansen
 */

@Service("userManager")
public class UserManagerImpl extends GenericEntityManagerImpl<User> implements
        UserManager {

    private UserDao userDao;

    private UserRoleDao userRoleDao;

    private BookSetDao bookSetDao;

    @Autowired
    public void setBookSetDao(BookSetDao bookSetDao) {
        this.bookSetDao = bookSetDao;
    }

    /**
     * Set the dao
     *
     * @param userDao the user dao to set
     */
    @Autowired
    public void setUserDao(UserDao userDao) {
        super.setEntityDao(userDao);
        this.userDao = userDao;
    }

    /**
     * Set the dao
     *
     * @param userRoleDao the userRole dao to set
     */
    @Autowired
    public void setUserRoleDao(UserRoleDao userRoleDao) {
        this.userRoleDao = userRoleDao;
    }

    /**
     * Will return the user based on the username
     *
     * @param username the username
     * @return the user information if available
     */
    public User get(String username) {
        return userDao.get(username);
    }

    /**
     * Get all user roles
     *
     * @return the user roles
     */
    public Collection<UserRole> getAllRoles() {
        return userRoleDao.getAll();
    }

    public User findByOpenIdLogin(String openIdLogin) {
        return userDao.get(openIdLogin);
    }

    @Override
    public void save(User entity) {
        bookSetDao.save(entity.getBooks());
        super.save(entity);
    }

    @Override
    public void update(User entity) {
        bookSetDao.update(entity.getBooks());
        super.update(entity);
    }
}
