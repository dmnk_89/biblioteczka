package pl.pw.bielwiernicki.manager;

import pl.pw.bielwiernicki.model.Organisation;

/**
 * Created with IntelliJ IDEA.
 * User: wiernicd
 * Date: 04.12.12
 * Time: 17:47
 * To change this template use File | Settings | File Templates.
 */
public interface OrganisationManager extends GenericEntityManager<Organisation>{
}
