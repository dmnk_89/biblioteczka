package pl.pw.bielwiernicki.manager;

import pl.pw.bielwiernicki.model.Book;
import pl.pw.bielwiernicki.model.BookSet;
import pl.pw.bielwiernicki.model.CopyOfBook;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;


public interface BookManager extends GenericEntityManager<Book> {
    public List<Book> find(String input);

    public List<CopyOfBook> getAllCopies();

    public Collection<BookSet> getAllBookSets();

    void save(CopyOfBook modelObject);

    public BookSet getBookSet(BookSet books);
}
