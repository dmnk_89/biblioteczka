package pl.pw.bielwiernicki.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CopyOfBook extends ParentModel {

    private Book book;

    private Integer numberOfCopies;

    private Visibility visibility;

    public CopyOfBook(Book book) {
        this.book = book;
        numberOfCopies = 1;
        visibility = Visibility.EVERYONE;
    }

    protected CopyOfBook() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public CopyOfBook(Book book, int numberOfCopies) {
        this.book = book;
        this.numberOfCopies = numberOfCopies;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Integer getNumberOfCopies() {
        return numberOfCopies;
    }

    public void setNumberOfCopies(Integer numberOfCopies) {
        this.numberOfCopies = numberOfCopies;
    }

    public Visibility getVisibility() {
        return visibility;
    }

    public void setVisibility(Visibility visibility) {
        this.visibility = visibility;
    }

    @Override
    public void prePersist() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void increment() {
        numberOfCopies++;
    }

    public void swapVsibility() {
        if (visibility == Visibility.EVERYONE) {
            visibility = Visibility.NOBODY;
        } else {
            visibility = Visibility.EVERYONE;
        }
    }

    public void decrement() {
        numberOfCopies--;
    }

    public void increment(int numberOfCopies) {
        this.numberOfCopies += numberOfCopies;
    }


}
