package pl.pw.bielwiernicki.model;

import javax.persistence.*;
import java.util.*;


@Entity
public class BookSet extends ParentModel implements Iterable<CopyOfBook>{
    @OneToMany   (cascade = CascadeType.ALL,orphanRemoval = true, fetch = FetchType.EAGER)
    @MapKeyColumn(name = "book_id")
    private Map<Long, CopyOfBook> books= new HashMap<Long, CopyOfBook>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Long getId() {
        return id;
    }

    @Override
    public void prePersist() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Iterator<CopyOfBook> iterator() {
        return books.values().iterator();
    }

    public int size() {
        return books.size();  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean isEmpty() {
        return books.isEmpty();  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean contains(Object o) {
        return books.containsValue(o);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public boolean remove(Object o) {
        if(o instanceof Book){
            books.remove(((Book) o).getId());
        }else if(o instanceof CopyOfBook){
            books.remove(((CopyOfBook) o).getBook().getId());
        }
        return false;
    }

    public boolean add(CopyOfBook copyOfBook) {
        return add(copyOfBook.getBook());
    }
    public boolean add(Book modelObject) {
        return add(modelObject,1);
    }
    public boolean add(Book book, Integer n) {
        if (books.containsKey(book.getId())) {
            books.get(book.getId()).increment(n);
        } else {
            books.put(book.getId(), new CopyOfBook(book));
        }
        return true;
    }

    public CopyOfBook get(Book book) {
        return books.get(book.getId());
    }


    public void delete(Book book) {
        CopyOfBook copy = books.get(book.getId());
        if(copy == null) return;

        copy.decrement();
        if(copy.getNumberOfCopies()<=0){
           books.remove(book.getId());
        }
    }

    public Collection<CopyOfBook> getColecton(){
        return books.values();
    }

}
