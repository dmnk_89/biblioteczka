package pl.pw.bielwiernicki.model;


public enum Visibility {
    EVERYONE, NOBODY
}
