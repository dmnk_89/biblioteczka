package pl.pw.bielwiernicki.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @author rjansen
 */
@Entity
@SuppressWarnings("serial")
public class User extends ParentModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private String username;

    private String mail;

    private String name;


    private BookSet books;

//    @ElementCollection
//    //@javax.persistence.MapKey(name = "book")
//    private Map<Book, Integer> books;


    /**
     * The default constructor
     */
    public User() {
        super();
        if (books == null)

            this.books = new BookSet();

    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }


    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Override
    public void prePersist() {
        // Do nothing for this domain model object
    }

    @Override
    public String toString() {
        return username + " : " + mail;
    }

    public String getFullName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public BookSet getBooks() {
        return books;
    }

    public void setBooks(BookSet books) {
        this.books = books;
    }
}