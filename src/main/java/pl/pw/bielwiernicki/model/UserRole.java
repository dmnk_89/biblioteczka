package pl.pw.bielwiernicki.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @author rjansen
 */
@SuppressWarnings("serial")
@Entity
public class UserRole extends ParentModel implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private String role;

    /**
     * Role description
     */
    private String description;

    /**
     * Default constructor
     */
    public UserRole() {
        super();
    }

    /**
     * Constructor for a user role
     *
     * @param role the rolename itself
     */
    public UserRole(String role) {
        this.role = role;
    }

    /**
     * @return the role
     */
    public String getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public void prePersist() {
    }

}
