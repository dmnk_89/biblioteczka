package pl.pw.bielwiernicki.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created with IntelliJ IDEA.
 * User: wiernicd
 * Date: 04.12.12
 * Time: 17:37
 * To change this template use File | Settings | File Templates.
 */
@Entity
public class Organisation extends ParentModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Long getId() {
        return id;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public void prePersist() {
        //nothing so far
    }
}
