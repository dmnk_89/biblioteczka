package pl.pw.bielwiernicki.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Book extends ParentModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToMany(fetch = FetchType.EAGER)
    private List<Author> authors;


    private String title;

    public Book() {
        authors = new ArrayList<Author>();
    }

    public Book(String title) {
        this.title = title;
    }


    /**
     * @return The id (primary key) of the entity.
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id The id (primary key) to be set.
     */
    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void addAuthor(Author author) {
        authors.add(author);

    }


    @Override
    public void prePersist() {
        //nothing
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        if(title!=null)
        return title;
        return "no name";
    }
}
